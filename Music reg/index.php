<?php
session_start();
include_once 'music.php';
$musicList = $songs;
$filter="pop";
?>
 <!DOCTYPE html>
 <html lang="en">
 <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <link rel="stylesheet" href="style.css">
    <title>Music App</title>
 </head>
 <body>
    <header>
        <div class="menu_side">
            <h1>Playlist</h1>
            <div class="playlist">
                <h4 class="active"><span><i class="bi bi-music-note-beamed"></i> Playlist</span></h4>
                <!-- <h4><span><i class="bi bi-music-note-beamed"></i> Last Listening</span></h4> -->
                <!-- <h4><span><i class="bi bi-music-note-beamed"></i> Recommended</span></h4> -->
            </div>
            <div class="menu_song">
                <?php
                $displayList = $musicList;
                $len = count($displayList);
                $i = 1;
                $filter = $_SESSION['filter'];
                // $len = 0;
                if ($len == 0) {
                    echo '<li class="songItem">
                               No Song
                            </li>';
                } else {
                    foreach ($musicList as $song) {  
                        echo '<li class="songItem" data-song-path="'.$song["path"].'"  data-image-path="'.$song["image"].'" >
                                <span>'.$i.'</span>
                                <img src="'.$song['image'].'" alt="'.$song['name'].'">
                                <h5>
                                '.$song['name'].'
                                    <div class="subtitle">'.$song['by'].'</div>
                                    <i class="bi playListPlay bi-play-circle-fill" id="'.$i.'"></i>
                                </h5>
                                <i class="bi playListPlay bi-play-circle-fill" id="'.$i.'"></i>
                            </li>';
                            $i++;
                    }
                }
                
                
                ?>
            </div>
        </div>

        <div class="song_side">
            <nav>
                <ul>
                    <li>Discover <span></span></li>
                    <!-- <li>MY LIBRARY</li> -->
                    <!-- <li>RADIO</li> -->
                </ul>
                <form action="" method="post" style="width:300px">
                <div class="search" style="width:300px">
                <!-- <input type="submit" value=""> -->
                    <i class="bi bi-search" style="color: white;"></i>
                    <input type="" value="<?php echo $searching;?>" name="searching" placeholder="search a music..." style="height:30px;">
                </div>
                <!-- </form> -->
            </nav>
            <div>
                <?php
                    $searching = '';
                    if ($_POST) {
                        if ($_POST['searching']) {
                            $searching = $_POST['searching'];
                            var_dump($searching);
                        }
                        
                    }
                    if (!empty($searching)) {
                        echo ' 
                        <div class="popular_song" style="color: white;">
                            <div class="h4">
                                <h4>Search Result</h4>
                                <div class="btn_s">
                                    <i id="left_scroll" class="bi bi-arrow-left-short"></i>
                                    <i id="right_scroll" class="bi bi-arrow-right-short"></i>
                                </div>
                            </div>
                            <div class="pop_song">';
                                        $i=1;
                                        $j=0;
                                        // $searching = 'ong';
                                        foreach ($musicList as $song) {  
                                            if (strpos($song['name'],$searching)) {
                                                echo '<li class="songItem" data-song-path="'.$song["path"].'" data-image-path="'.$song["image"].'" >
                                                        <div class="img_play">
                                                            <img src="'.$song['image'].'" alt="'.$song['name'].'" width="20px" height="20px">
                                                            <i class="bi playListPlay bi-play-circle-fill" id="'.$i.'"></i>
                                                        </div>
                                                        <h5>'.$song['name'].'
                                                            <div class="subtitle">'.$song['by'].'</div>
                                                        </h5>
                                                    </li>';
                                                    if ($i==7) {
                                                        break;
                                                    }
                                                $i++;
                                                $j++;
                                            }
                                        }
                                        if ($j == 0) {
                                            echo '<li class="songItem" >
                                                       <h1>No Music Found</h1>
                                                    </li>';
                                        }
                            echo '
                                </div>
                            </div>';
                    } else {
                        echo ' 
                        <div class="popular_song" style="color: white;">
                            <div class="h4">
                                <h4>Popular Song</h4>
                                <div class="btn_s">
                                    <i id="left_scroll" class="bi bi-arrow-left-short"></i>
                                    <i id="right_scroll" class="bi bi-arrow-right-short"></i>
                                </div>
                            </div>
                            <div class="pop_song">';
                                    $i=1;
                                    foreach ($musicList as $song) {  
                                        if ($song['status']=="Popular") {
                                            echo '<li class="songItem" data-song-path="'.$song["path"].'" data-image-path="'.$song["image"].'" >
                                                    <div class="img_play">
                                                        <img src="'.$song['image'].'" alt="'.$song['name'].'" width="20px" height="20px">
                                                        <i class="bi playListPlay bi-play-circle-fill" id="'.$i.'"></i>
                                                    </div>
                                                    <h5>'.$song['name'].'
                                                        <div class="subtitle">'.$song['by'].'</div>
                                                    </h5>
                                                </li>';
                                                if ($i==7) {
                                                    break;
                                                }
                                            $i++;
                                        }
                                    }
                        echo '
                            </div>
                        </div>
                        <div class="popular_song" style="color: white;">
                            <div class="h4">
                            <h4>Genre</h4>
                                <nav>
                                <form action="" method="post">
                                    <ul>
                                        <li><input type="submit" name="changeGenre" value="pop"/>'.($filter =="pop"? "<span></span>":"").'</li>
                                        <li><input type="submit" name="changeGenre" value="dance"/>'.($filter =='dance'? "<span></span>":"" ).'</li>
                                        <!-- </form> -->
                                    </ul>
                                </nav>
                            </div>
                        <div class="pop_song">';
                                    $filter="pop";
                                    if ($_POST) {
                                    if ($_POST['changeGenre']) {
                                        $filter = $_POST['changeGenre'];
                                        $_SESSION['filter'] = $_POST['changeGenre'];
                                    }
                                    }
                                    $i=1;
                                    $j=0;
                                    foreach ($musicList as $song) {
                                        if (in_array($filter, $song['genre'])) {
                                            echo '<li class="songItem" data-song-path="'.$song["path"].'" data-image-path="'.$song["image"].'" >
                                                    <div class="img_play">
                                                        <img src="'.$song['image'].'" alt="'.$song['name'].'" width="20px" height="20px">
                                                        <i class="bi playListPlay bi-play-circle-fill" id="'.$i.'"></i>
                                                    </div>
                                                    <h5>'.$song['name'].'
                                                        <div class="subtitle">'.$song['by'].'</div>
                                                    </h5>
                                                </li>';
                                                if ($i==7) {
                                                    break;
                                                }
                                            $i++;
                                            $j++;
                                        }
                                    }
                                    if ($j == 0) {
                                        echo '<li class="songItem" >
                                                No Song under this Genre
                                                </li>';
                                    }
                            echo
                            '</div>
                        </div>';
                    }
                    
                ?>
                
            </div>
            <div class="music-player">
                    <img id="playerImage" src="image/test.png" alt="Music Player Image" class="player-image">  
                    <audio id="player" controls style="width:75%;">
                        <source src="music.mp3" type="audio/mpeg">
                        Your browser does not support the audio element.
                    </audio>
            </div>
        </div>
        
    </header>
    <!-- <?php 
    var_dump($musicList);
    ?> -->
    
    <script src="app.js"></script>
    <script>
        const audio = document.getElementById("player"); // Assuming you have an audio element with id="player"
        const image = document.getElementById("playerImage");
        const songButtons = document.querySelectorAll(".songItem");

        songButtons.forEach(button => {
            button.addEventListener("click", () => {
            const songPath = button.dataset.songPath;  // Access data-song-path
            const imagePath  = button.dataset.imagePath;
            audio.src = songPath;
            image.src = imagePath;
            audio.load(); // Optional: preload the audio file
            audio.play();
            });
        });
    </script>
</body>
 </html>