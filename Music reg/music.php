<?php
$songs = [
    [
        "name" => "Song 1",
        "desc" => "This is a great song",
        "by" => "test 1",
        "year" => 2020,
        "genre" => ["dance"],
        "status" => "Recommended",
        "image" => "image/test.png",
        "path" => "music/music.mp3"
    ],
    [
        "name" => "Song 2",
        "desc" => "A mellow track",
        "by" => "test 1",
        "year" => 2021,
        "genre" => ["dance"],
        "status" => "Last Listening",
        "image" => "image/test2.png",
        "path" => "music/music2.mp3"
    ],
    [
        "name" => "Song 3",
        "desc" => "A catchy tune",
        "by" => "test 2",
        "year" => 2023,
        "genre" => ["pop"],
        "status" => "Popular",
        "image" => "image/test3.png",
        "path" => "music/music3.mp3"
    ],
    [
        "name" => "Song 4",
        "desc" => "An upbeat song",
        "by" => "test 2",
        "year" => 2019,
        "genre" => ["dance"],
        "status" => "Recommended",
        "image" => "image/test4.png",
        "path" => "music/music4.mp3"
    ],
    [
        "name" => "Song 5",
        "desc" => "A relaxing song",
        "by" => "test 3",
        "year" => 2022,
        "genre" => ["pop"],
        "status" => "Last Listening",
        "image" => "image/test5.png",
        "path" => "music/music5.mp3"
    ],
    [
        "name" => "Song 6",
        "desc" => "A powerful song",
        "by" => "test 3",
        "year" => 2018,
        "genre" => ["pop"],
        "status" => "Popular",
        "image" => "image/test6.png",
        "path" => "music/music6.mp3"
    ],
    [
        "name" => "Song 7",
        "desc" => "A beautiful song",
        "by" => "test 4",
        "year" => 2024,
        "genre" => ["pop"],
        "status" => "Recommended",
        "image" => "image/test.png",
        "path" => "music/music.mp3"
    ],
    [
        "name" => "Song 8",
        "desc" => "A nostalgic song",
        "by" => "test 4 ",
        "year" => 2017,
        "genre" => ["pop"],
        "status" => "Last Listening",
        "image" => "image/test3.png",
        "path" => "music/music3.mp3"
    ],
    [
        "name" => "Song 9",
        "desc" => "A dance song",
        "by" => "test 5",
        "year" => 2020,
        "genre" => ["pop","dance"],
        "status" => "Popular",
        "image" => "image/test2.png",
        "path" => "music/music2.mp3"
    ],
    [
        "name" => "Song 10",
        "desc" => "A ballad",
        "by" => "test 5",
        "year" => 2021,
        "genre" => ["pop"],
        "status" => "Recommended",
        "image" => "image/test5.png",
        "path" => "music/music5.mp3"
    ]
];
function getAllSong() {
    
    
    return $songs;
}
?>